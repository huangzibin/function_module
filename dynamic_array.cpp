/********************************************************************************
**  (C) COPYRIGHT 2018 Tongda Electric CO.LTD 
**  author        :  hzb
**  version       :  v1.0
**  date           :  2018.01.16
**  description  : 可动态增长的数组，非线程安全。
					调用初始化函数前必须保证结构体已清零
********************************************************************************/
	
struct dynamic_array {
	int size;
	int elem_size;
	char* buffer;
	int init_flag;
};

#if 0
struct dynamic_array *create_dynamic_array(int malloc_count, int elem_size)
{
	if (malloc_count <= 0 || elem_size <= 0) {
		fprintf(stderr, "argument invaild malloc_count(%d), elem_size(%d)\r\n", malloc_count, elem_size);
		return -1;
	}
	int darray_size = sizeof (struct dynamic_array); 
	struct dynamic_array *array = malloc(darray_size);
	if ()
	memset()
}
#endif

int dynamic_array_init(struct dynamic_array* array, int size, int elem_size)
{
	if (!array) {
		fprintf(stderr, "array == NULL\r\n");	
		return -1;
	}
	if (size <= 0) {
		fprintf(stderr, "size(%d)  <= 0\r\n");	
		return -1;
	}
	if (elem_size <= 0) {
		fprintf(stderr, "elem_size(%d)  <= 0\r\n");	
		return -1;
	}
	memset(array, 0, sizeof(struct dynamic_array));
	int buf_size = size*elem_size;
	array->buffer = malloc(buf_size);
	if (array->buffer == NULL) {
		fprintf(stderr, "malloc array failed\r\n", strerror(errno));
		return -1;
	}
	memset(array->buffer, 0, buf_size);
	array->size = size;
	array->elem_size = elem_size;
	array->init_flag = 1;
	return 0;
}

void dynamic_array_deinit(struct dynamic_array* array)
{
	if (!array)
		return;
	if (array->init_flag == 0) {
		return ;
	}
	free(array->buffer);
	return ;
}

#define CHECK_ARRAY_NULL(array) if (!(array)) {fprintf(stderr, "array == NULL\r\n");return -1;}
#define CHECK_ARRAY_INIT(array) if (!(array)->init_flag) {fprintf(stderr, "array has not inited\r\n");return -1;}

#define CHECK_ARRAY_NULL2(array) if (!(array)) {fprintf(stderr, "array == NULL\r\n");return NULL;}
#define CHECK_ARRAY_INIT2(array) if (!(array)->init_flag) {fprintf(stderr, "array has not inited\r\n");return NULL;}

int dynamic_array_resize(struct dynamic_array* array, int size)
{
	CHECK_ARRAY_NULL(array);
	CHECK_ARRAY_INIT(array);
	if (size <= array->size) {
		fprintf(stderr, "not support trim array\r\n");
		return 0;
	}
	int buf_size = size * array->elem_size;
	char* buf_tmp = realloc(array->buffer, buf_size);
	if (!buf_tmp) {
		fprintf(stderr, "array has not inited\r\n");
		return -1;
	}
	array->buffer = buf_tmp;
	array->size = size;
	return 0;
}

int dynamic_array_inited(struct dynamic_array* array)
{
	CHECK_ARRAY_NULL(array);
	return array->init_flag;
}

int dynamic_array_size(struct dynamic_array* array)
{
	CHECK_ARRAY_NULL(array);
	CHECK_ARRAY_INIT(array);
	return array->size;
}

void* dynamic_array_get_index(struct dynamic_array* array, int index)
{
	CHECK_ARRAY_NULL2(array);
	CHECK_ARRAY_INIT2(array);
	if (index < 0 || index >= array->size) {
		fprintf(stderr, "index(%d) is not vaild, should between %d-%d\r\n", 0, array->size);
		return NULL;
	}
	return array->buffer+index*array->elem_size;
}