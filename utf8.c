#if 1
#include <stdio.h>
#include <string.h>

int GetUtf8Length(char const *str, int* char_num, int* one_byte_char_num,
							int *two_byte_char_num, int* three_byte_char_num, int* four_byte_char_num)
{
	int total_num_tmp = 0;
	int one_byte_char_num_tmp = 0;
	int two_byte_char_num_tmp = 0;
	int three_byte_char_num_tmp = 0;
	int four_byte_char_num_tmp = 0;
	if (!str) {
		return -1;
	}
	int length = strlen(str);
	int index = 0;
	while( str[index] != '\0' && index < length){
		if( str[index] & 1<<7 ){/* 第8位(从右到左)为1*/
			if( str[index] & (1<<6) ){/*第7位为1*/
				if( str[index] & (1<<5) ){/*第6位为1*/
					if( str[index] & (1<<4)){/*第5位为1  11110xxx*/
						four_byte_char_num_tmp++,total_num_tmp++,index += 4;/*4字节的字符总数加1,各种长度总数加1,指针向后移动4位*/
						continue;/*开始下次循环*/
					}
 
					three_byte_char_num_tmp++,total_num_tmp++,index += 3;/* 1110xxxx*/
					continue;
 
				}
 
				two_byte_char_num_tmp++,total_num_tmp++,index += 2;/*110xxxxx*/
				continue;
 
			}
		}
 
		one_byte_char_num_tmp++,total_num_tmp++,index += 1;/*0xxxxxxx*/
		continue;
 
	}
	if (char_num)
		*char_num = total_num_tmp;
	if (one_byte_char_num)
		*one_byte_char_num = one_byte_char_num_tmp;
	if (two_byte_char_num)
		*two_byte_char_num = two_byte_char_num_tmp;
	if (three_byte_char_num)
		*three_byte_char_num = three_byte_char_num_tmp;
	if (four_byte_char_num)
		*four_byte_char_num = four_byte_char_num_tmp;
	return 0;
}	

int main()
{
	const char *str = "撒的发#@！#生的aaaaa的说法";
	int total_len = 0;
	int one = 0;
	int two = 0;
	int three = 0;
	int four = 0;
	GetUtf8Length(str, &total_len, &one, &two, &three, &four);
	printf("%d %d %d %d %d\n", total_len, one, two, three, four);
	return 0;
}
#else

# include <stdio.h>
# include <stdlib.h>
# include <string.h>/*不用memset函数，可以不要*/
 
void GetUtf8Length( char const *str, int *cnt );
 
/******************************************MAIN******************************************************/
int
main( void ){
	const char *str = "撒的发#@！#生的aaaaa的说法";
 
	//char str[] = "这是一条UTF-8的文本，请数数aaaabbbbcccc*(是的.";
	int cnt[5];
	memset( cnt, 0, sizeof(int) * 4 );
	GetUtf8Length( str, cnt);
	printf( " 这条文本的字符共%d个 ; 其中汉字有%d个, 英文字符%d个\n", cnt[0], cnt[3], cnt[1] );
 
	return EXIT_SUCCESS;
}
/******************************************END*******************************************************/
 
/******************************************FUNC******************************************************/
void
GetUtf8Length( char const *str, int *cnt){
 
	while( *str != '\0' ){
		if( *str & 1<<7 ){
			if( *str & (1<<6) ){
				if( *str & (1<<5) ){
					if( *str & (1<<4)){
						cnt[4]++,cnt[0]++,str += 4;
						continue;
					}
					cnt[3]++,cnt[0]++,str += 3;
					continue;
				}
				cnt[2]++,cnt[0]++,str += 2;
				continue;
			}
		}
		cnt[1]++,cnt[0]++,str += 1;
		continue;
	}
}
#endif
