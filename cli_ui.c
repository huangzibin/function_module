#include <stdio.h>

void printf_help_info()
{
	printf("\r\n");
	printf("q exit\r\n");
	printf("1.test1\r\n");
	printf("2.test2\r\n");
	return;
}

static int test_choice(void)
{
	//printf("choose:[ 1-6, q = quit, h = help ]\r\n");
	char c[10] = {};
	fgets(c, 9, stdin);
	return c[0];
}

int test_menu()
{
	int exit_flag = 0;
	while(!exit_flag)
	{
		int c = test_choice();
		switch(c)
		{
			case 'q':
				exit_flag = 1;
				break;
			case '1':
				printf("test1\n");
				break;
			case '2':
				printf("test2\n");
				break;
			case 'h':
			case '\n':
				printf_help_info();
				break;
			default:
				fprintf(stderr, "invalid choose\n");
				break;
		}
	}

	return 0;
}

int main()
{

	test_menu();
	return 0;
}
