#ifndef __CMD_OPTION_H__
#define __CMD_OPTION_H__

#ifdef __cplusplus
	extern "C" {
#endif


int option_add(char* option_name, const char* option_desc, char* buf_ptr, int buf_size);
void option_show_usage();
int option_parse(int argc, char** argv);


#ifdef __cplusplus
	}
#endif

#endif
