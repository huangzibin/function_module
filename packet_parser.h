#ifndef PACKET_PARSER_H
#define PACKET_PARSER_H


typedef int IsPacketVaild(const char* packet_buf, int buf_size, int* packet_size);

struct PacketParser {
	int fd;
	char* buf;
	int buf_size;
	int cur_data_len;
	char packet_start_code[10];
	int start_code_len;
};

enum PacketParserResult {
	PRESULT_VALID = 0,
	PRESULT_INVALID = 1,
	PRESULT_INCOMPLETE = 2,
};

int packet_parser_init(struct PacketParser* packet_parser, int fd, int max_packet_size,
						const char* packet_start_code, int startcode_len);
void packet_parser_deinit(struct PacketParser* packet_parser);
int packet_parser_get_one_packet(struct PacketParser* packet_parser, char* buf, int buf_size, int *size, int *end_flag, 
										IsPacketVaild* is_packet_valid);
void packet_parser_set_fd(struct PacketParser* packet_parser, int fd);
void packet_parser_clear_buf(struct PacketParser* packet_parser);


#endif
