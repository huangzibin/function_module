read_config_file:read_config_file.c output_dir
	gcc read_config_file.c  -o $@ -Wall 
	mv $@ output_dir/

cli_ui:cli_ui.c output_dir
	gcc cli_ui.c  -o $@ -Wall 
	mv $@ output_dir/

LinkList:LinkList.c LinkList.h test_linklist.c output_dir
	gcc LinkList.c LinkList.h test_linklist.c  -o $@ -Wall 
	mv $@ output_dir/

utf8:utf8.c output_dir
	gcc utf8.c -g  -o $@ -Wall 
	mv $@ output_dir/

QUEUE_SRC=queue.c queue.h LinkList.c test_queue.c
QUEUE_OBJ=$(patsubst %.c,%.o,$(wildcard $(QUEUE_SRC)))
queue: $(QUEUE_OBJ) output_dir
	gcc $(QUEUE_OBJ) -o $@ -Wall 
	mv $@ output_dir/

PACKETPARSER_SRC=packet_parser.c packet_parser.h
PACKETPARSER_OBJ=$(patsubst %.c,%.o,$(wildcard $(PACKETPARSER_SRC)))
packetparser: $(PACKETPARSER_OBJ) output_dir
	gcc $(PACKETPARSER_OBJ) -o $@ -Wall -Werror -g
	mv $@ output_dir/

%o: %c
	gcc -o $@ -c $< -g -Werror -Wall

#%o: %c
        #gcc -g -c $^  -o $@ -Wall 

output_dir:
	mkdir output_dir

clean:
	 rm *.o
